# OPTC - Stamina Manager

### Hosted online
Available at: https://drill.gitlab.io/OPTC

---

### Description
Tool to manage and save stamina in One Piece Treasure Cruise

### Languages supported
<ul><li>English</li><li>Italiano</li></ul>

### Example of use
Tomorrow there will be an important Raid Boss and you want to use all possible stamina on it.<br/>
You can play starting from 12:00, so you have to set your <b>maximum stamina</b> and your <b>start time</b>.<br/>
Now click the <b>first button</b>: this is the <b>time at which you will need to have 0 stamina</b> and stop playing.<br/>
<br/>
If the time at which you have 0 stamina was different from the one shown, click the <b>second button</b> to recompute the time at which you will have full stamina (shown in <b>"Event Start Time"</b>).<br/>
<br/>
<br/>
Now, if you have more stamina than necessary and you don't want to waste it, take a look at the <b>green box</b>:<br/>
here you can insert your <b>current stamina</b> and the <b>current time</b> and compute how much <b>stamina you have to use</b>.<br/>
<br/>
<br/>
Cool, right?<br/>
<br/>

---

<b><i>Tips</i></b>: you can set manually the <b>current time</b>, or whichever time you want, but if you don't do so, the <b>system time</b> is automatically set.<br/>
<b>Max Stamina</b> and <b>Language</b> are saved in the <b>browser's local storage</b>, so you don't need to set them everytime you use this tool.<br/>

---

<br/>
Here you can see an example of what has just been explained:<br/>
<br/>

![OPTC_screenshot](https://gitlab.com/Drill/OPTC/uploads/99dbd4c68370a3ff1bb3d6a22f854e04/OPTC-tool.PNG)
